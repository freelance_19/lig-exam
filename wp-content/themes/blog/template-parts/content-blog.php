<?php
/**
 * Template part for displaying posts archive
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package blog
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<a href="<?php echo get_permalink(); ?>">
		<?php blog_post_thumbnail(); ?>
		<div class="article-content">
			<?php if ( is_singular() ) :
				the_title( '<h2 class="entry-title">', '</h2>' );
			else :
				the_title( '<h2 class="entry-title">', '</h2>' );
			endif;
			
		if ( 'post' === get_post_type() ) :
				?>
				<div class="entry-meta">
					<?php
					blog_posted_on();
					?>
				</div><!-- .entry-meta -->
			<?php endif; ?>

		</div><!-- .article-content -->
	</a>
</article><!-- #post-<?php the_ID(); ?> -->
