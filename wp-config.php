<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lig-exam');

/** MySQL database username */
define('DB_USER', 'vdave');

/** MySQL database password */
define('DB_PASSWORD', 'tester1');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '&g[7}hv_<o56J{2(+7)0LRxqyjE{ImeN F}`vVE)!S@Sloi=lD?}Rqlj~{_K[s[I');
define('SECURE_AUTH_KEY',  'CwXE]_}8aWCCiF5=s^7]dSFdj^!ni:Y>[ d5JUO~9i<GUANbZboo^v}&m9^ %:D4');
define('LOGGED_IN_KEY',    '*-c l`y;7WH58 /#c=wGyED)RkO! Yb`c.w$+NdT1~,jD8KSqIOo3-y.qk@RAlf4');
define('NONCE_KEY',        'k4=)F>or#+3Jn0OAli+#0}BgpLKdA5CVTqK)OplCG-*lGF2Ns_wVHf6J~<ym{V:W');
define('AUTH_SALT',        'Y |c,7C0-}EK0Yl[h8(m.)9 y(Qv0t lfitS:2n#h2;E$O]A_ZeK.nu`kFmR4HIT');
define('SECURE_AUTH_SALT', 'w$1K9P;?<;xfV@:p@MUoyXl(:hvyn3cu_6;;n$bzKRocd/-p**<^^Xx?@X7WB5K&');
define('LOGGED_IN_SALT',   '<V7NB<bOH(FX~{TL,1tc#{i,Gn@}kg=dYMV7xGWn<(].K(Q%]9No`eQxfJ!d)>%o');
define('NONCE_SALT',       'sX&~HX[a_V?x]b%%n+SC+!lTiBu0Yuw6HVhn.73)318LVLA2K=a$n` 6/?vhvy&:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
