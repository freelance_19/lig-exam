# Bitbucket Git Repository: git clone git@bitbucket.org:freelance_19/lig-exam.git

# Wordpress 5.0.3

# WP URL setting: http://localhost/lig-exam

# Fonts Used
Primary: Helvetica
Secondary: Arial

# Plugins Used
1. wp-paginate
2. wpfront-scroll-top
3. font-organizer

# Theme Used
Folder name: blog

# Database
1. db name: lig-exam
2. character set: utf8mb4
3. collation: utf8mb4_general_ci
4. db file(root folder): lig-exam.sql

# Check also .gitignore file for missing folder(s)/file(s)